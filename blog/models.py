from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User 
from django.urls import reverse

#The post model and the user model are going to have a one-to-many realationship
#(one user can have multiple posts and a post has only one User - in Django we use 
#a foreign key)

class Post(models.Model):
	title = models.CharField(max_length=100)
	content = models.TextField()
	date_posted = models.DateTimeField(default=timezone.now)
	author = models.ForeignKey(User, on_delete=models.CASCADE) # on_delete = When a user is delete we delete and the posts

	def __str__(self):
		return self.title


	def get_absolute_url(self):
		return reverse('post-detail', kwargs={'pk': self.pk}) #returns the full path in a string
